import requests

print("loading pdp framework")
# load pdp framework
framework_version = "1e8a74cc039fad4f17bf890d28176b89cdc7142a"
exec(
    requests.get(
        f"https://bitbucket.org/gchen1016/pdp/raw/{framework_version}/pdpframework.py"
    ).text
)
print(f"load pdp framework success {framework_version}")
# docs: https://bitbucket.org/gchen1016/pdp/src/master

downloader = Downloader()


@downloader.source("^file1$")
class File1(Requests):
    def post_download(self, downloaded_filepath):
        with open(downloaded_filepath, "r") as fpr:
            html_str = fpr.read()
        from lxml import html, etree
        from lxml.builder import E

        html_root = html.fromstring(html_str)
        # merge two tables
        tables = html_root.xpath("//table")
        rows = tables[0].getchildren() + tables[1].getchildren()
        table = etree.Element("table")
        table.extend(rows)

        # use E to build html conveniently
        html_root = E.html(
            E.head(
                E.style(
                    """
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
"""
                )
            ),
            E.body(table),
        )

        with open(downloaded_filepath, "wb") as fpw:
            fpw.write(etree.tostring(html_root, pretty_print=True))


downloader.run()
