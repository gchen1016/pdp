import requests

print("loading pdp framework")
# load pdp framework
framework_version = "528066cb0fb7b7fe606263ac34a775a146adcf74"
exec(
    requests.get(
        f"https://bitbucket.org/gchen1016/pdp/raw/{framework_version}/pdpframework.py"
    ).text
)
print(f"load pdp framework success {framework_version}")
# docs: https://bitbucket.org/gchen1016/pdp/src/master

downloader = Downloader()


@downloader.source("^file1$")
class File1(Curl):
    xpath = "//p[contains(text(),'Hasil Lelang TD Valas Konvensional Overnight') or contains(text(),'Hasil Lelang TD Valas Konvensional ON')]//preceding::a[1]"

    def pre_download(self, metadata):
        import urllib.parse
        import urllib.request
        from lxml import html
        import os

        url = metadata["url"]

        # download main html in order to extract release links
        curl_cmd = f"curl -s {url} -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0' -o ./downloads/mainpage.html"
        os.system(curl_cmd)

        with open("./downloads/mainpage.html", "r") as fpr:
            html_doc = html.fromstring(fpr.read())
            link_nodes = html_doc.xpath(self.xpath)
            self.logger.info("link nodes: %s", link_nodes)
            metadata["url"] = list(
                map(lambda x: x.get("href").replace("\r\n", ""), link_nodes)
            )

        return metadata

    def post_download(self, downloaded_file_path):
        date_xpath = "//*[contains(@id,'layout-title')]"
        table_xpath = "(//table[contains(.,'Volume')])[last()]"
        from lxml import html

        with open(downloaded_file_path, "r") as fpr:
            html_str = fpr.read()
        html_doc = html.fromstring(html_str)
        date_str = html.tostring(html_doc.xpath(date_xpath)[0], pretty_print=True)
        table_str = html.tostring(html_doc.xpath(table_xpath)[0], pretty_print=True)
        new_html_str = b"<html><body>" + date_str + table_str + b"</body></html>"
        with open(downloaded_file_path, "wb") as fpw:
            fpw.write(new_html_str)


@downloader.source("^file2$")
class File2(File1):
    xpath = "//p[contains(text(),'Hasil Lelang TD Valas Konvensional Non Overnight') or contains(text(),'Hasil Lelang TD Valas Konvensional Non ON')]//preceding::a[1]"


@downloader.source("^file3$")
class File3(File1):
    xpath = "//p[contains(text(),'Hasil Lelang TD Valas Konvensional Syariah') or contains(text(), 'HASIL LELANG TD VALAS KONVENSIONAL SYARIAH')]//preceding::a[1]"


downloader.run()
