from pdpframework import *

downloader = Downloader()
files = {
    "table17.xlsx": {
        "url": "http://www.szse.cn/api/report/ShowReport?SHOWTYPE=xlsx&CATALOGID=1837_xxpl&txtDate={}&random={}&TABKEY=tab1"
    }
}


@downloader.source("^table17$")
class File1(Requests):
    def pre_download(self, metadata):
        self.headers.update(
            {"Referer": "http://www.szse.cn/disclosure/margin/margin/index.html"}
        )

        from datetime import datetime, timedelta
        from random import random

        last_10_days = map(
            lambda day: day.strftime("%Y-%m-%d"),
            map(lambda delta: datetime.today() - timedelta(days=delta), range(10)),
        )
        base_url = metadata["url"]
        metadata = list(
            map(
                lambda day: {
                    "url": base_url.format(day, random()),
                    "filename": f"table17_{day}.xlsx",
                },
                last_10_days,
            )
        )
        return metadata

    def post_download(self, fpath):
        print(fpath)


downloader.run(files)
