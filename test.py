from pdpframework import *

if __name__ == "__main__":
    # declare your files like in CDP or copperfield
    # if there's a non-empty option.json file
    # (which means dynamic mapping in BPA system)
    # then files will be read automatically so you don't have to manually set it
    files = {
        "file1.zip": {
            "url": "https://raw.githubusercontent.com/lucktu/n2n/master/Windows/n2n_v1_windows_x64_v1.3.2_r124_static_by_Tim.zip",
        },
        "file2.zip": {
            "url": "https://raw.githubusercontent.com/lucktu/n2n/master/Windows/n2n_v1_windows_x64_v1.3.2_r124_static_by_Tim.zip",
        },
        # "file3.zip": {
        #     "url":
        #     "https://raw.githubusercontent.com/lucktu/n2n/master/Windows/n2n_v1_windows_x64_v1.3.2_r124_static_by_Tim.zip",
        # }
    }

    # initiate downloader
    # like in CPY: parser = Parser()
    downloader = Downloader()

    # inherit Get class for HTTP GET request behaviour
    @downloader.source("^file1$")
    class File1(Requests, Unzip):
        # set headers, params, auth...etc simply
        # see `python requests` docs for details
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.82 Safari/537.36"
        }
        proxies = {"all": "http://127.0.0.1:1082"}

        # optional function to run before download start
        def pre_download(self, metadata):
            # do something before download,
            # like insert date string to url...
            print("metadata:", metadata)

            # self.session is like normal requests action except with headers, params set in this class
            # self.session.headers: {"User-agent": "Mozilla/5.0"}
            # you can do self.session.get ..etc like requests.get ..etc
            return metadata

        # # optional function to run after download
        # # commented out because of the Unzip class inheritance in File1
        # # has its own post_download to unzip files
        # def post_download(self, downloaded_file_path):
        #     # do something after download,
        #     # like convert json string to csv for parser
        #     # print("downloaded file path:", downloaded_file_path)
        #     pass

    @downloader.source("^file2$")
    class File2(File1):
        # set headers, params, auth...etc simply
        # see `python requests` docs for details
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.82 Safari/537.36jdidjfodjfj"
        }
        proxies = {"all": "http://127.0.0.1:1082"}

    @downloader.source("^file3$")
    class File3(Requests, Unzip):
        def pre_download(self, metadata):
            return metadata

    # can run in parallel
    downloader.run(files, parallel_num=1)
