from pdpframework import *
import os

# ATTENTION!! this download method currently only suitable for html page download!
if __name__ == "__main__":
    files = {
        "file1.htm": {
            "url": "http://www.nhc.gov.cn/xcs/yqjzqk/list_gzbd.shtml",
        }
    }
    downloader = Downloader()

    @downloader.source("^file1$")
    class File1(CloudPlaywright):
        cloud_playwright_api_url_list = [
            "https://normal-playwright-api-eloco.cloud.okteto.net/post",
        ]

        def post_download(self, downloaded_file_path):
            os.system(f"cat {downloaded_file_path}")

    downloader.run(files, parallel_num=1)
