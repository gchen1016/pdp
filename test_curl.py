from pdpframework import *

if __name__ == "__main__":
    files = {
        "file1.zip": {
            "url": "https://raw.githubusercontent.com/lucktu/n2n/master/Windows/n2n_v1_windows_x64_v1.3.2_r124_static_by_Tim.zip",
        }
    }
    downloader = Downloader()

    @downloader.source("^file1$")
    class File1(Curl, Unzip):
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.82 Safari/537.36",
        }

        def pre_download(self, metadata):
            print("metadata:", metadata)
            return metadata

    # can run in parallel
    downloader.run(files, parallel_num=1)
