from pdpframework import *

files = {
    "file1.zip": {
        "url": "https://raw.githubusercontent.com/lucktu/n2n/master/Windows/n2n_v1_windows_x64_v1.3.2_r124_static_by_Tim.zip",
    },
    "file2.zip": {
        "url": "https://raw.githubusercontent.com/lucktu/n2n/master/Windows/n2n_v1_windows_x64_v1.3.2_r124_static_by_Tim.zip",
    },
    "file3.zip": {
        "url": "http://127.0.0.1:8000?request_id={}",
    },
}

downloader = Downloader()
downloader.retry_delay = 5


@downloader.source("^file(1|2)$")
class File1(Curl, Unzip):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.82 Safari/537.36",
    }

    def pre_download(self, metadata):
        print("metadata:", metadata)
        assert 0
        return metadata


@downloader.source("^file3$")
class File1(Requests):
    def pre_download(self, metadata):
        self.headers.update(
            {"Referer": "http://www.szse.cn/disclosure/margin/margin/index.html"}
        )

        from datetime import datetime, timedelta
        from random import random

        nums = range(0, 10)
        base_url = metadata["url"]
        metadata = list(
            map(
                lambda num: {
                    "url": base_url.format(num),
                    "filename": f"file3_{num}.xlsx",
                },
                nums,
            )
        )
        print(metadata)
        return metadata

    def post_download(self, fpath):
        print(fpath)
        import time

        time.sleep(5)


downloader.run(files, parallel_num=2)
