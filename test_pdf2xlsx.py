from pdpframework import *

if __name__ == "__main__":
    files = {
        "file1.xlsx": {
            "url": "http://127.0.0.1:8004/tmp/file1.pdf",
        }
    }
    downloader = Downloader()

    @downloader.source("^file1$")
    class File1(Requests, Pdf2Xlsx):
        pdf2xlsx_flavor = "lattice"
        pdf2xlsx_lattice_line_scale = 70
        pdf2xlsx_min_number_count_of_a_data_row = 5

    downloader.run(files)
