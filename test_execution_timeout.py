from pdpframework import *
import time

# ATTENTION!! this download method currently only suitable for html page download!
if __name__ == "__main__":
    files = {
        "file1.htm": {
            "url": "http://www.baidu.com",
        }
    }
    downloader = Downloader()

    @downloader.source("^file1$")
    class File1(Requests):
        def post_download(self, downloaded_file_path):
            time.sleep(10)
            print("run finished")

    downloader.execution_timeout = 5
    downloader.run(files, parallel_num=1)
