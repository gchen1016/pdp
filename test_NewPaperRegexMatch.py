from glob import glob

from pdpframework import *

if __name__ == "__main__":
    files = {
        "file1.csv": {
            "url": "./tmp/file1.htm.0",
        },
    }
    downloader = Downloader()

    @downloader.source("^file1$")
    class File1(LocalFile, NewsPaperRegexMatch):
        def pre_download(self, metadata):
            print(metadata)
            metadata = [
                metadata,
                {"filename": "file1_1.csv", "url": "./tmp/file1.htm.1"},
            ]
            return metadata

        import re

        regex_match_flags = re.DOTALL | re.MULTILINE

        # desc starts with __ will be ignored in output desc
        desc_regex_dict = {
            ("date", r"截至.*?(\d+月\d+日)"): {},
            ("__目标段落", r".*(截至.*报告.*接种.*新冠.*疫苗.*接种总人数.*)"): {
                ("test_hahahahahah", r"^haha$"): {},
                ("__60岁以下", r"(.*)60岁以上.*"): {
                    (
                        "接种总人数",
                        (
                            r"接种总人数123[\u4e00-\u9fa5 ]*?(\d.*?万)",
                            r"接种总人数12.*?(\d.*?万)",
                            r"接种总人数1.*?(\d.*?万)",
                            r"接种总人数xxx.*?(\d.*?万)",
                        ),
                    ): {},
                    ("完成全程接种", r"完成全程接种.*?(\d.*?万)"): {},
                    ("加强免疫接种", r"加强免疫接种.*?(\d.*?万)"): {},
                    ("序贯加强免疫接种", r"序贯加强免疫接种.*?(\d.*?万)"): {},
                    ("覆盖率", r"覆盖人数和全程接种人数分别.*?(\d.*%).*?\d.*%"): {},
                    ("全程接种率", r"覆盖人数和全程接种人数分别.*?\d.*%.*?(\d.*%)"): {},
                    ("haha1", r"^123$"): {},
                    ("haha2", ("", "haha")): {},
                    ("haha3", ()): {},
                },
                ("60岁以上", r".*(60岁以上.*)"): {
                    ("接种覆盖人数", r"接种覆盖人数.*?(\d.*?万)"): {},
                    ("完成全程接种", r"完成全程接种.*?(\d.*?万)"): {},
                    ("覆盖率", r"覆盖人数和全程接种人数分别.*?(\d.*%).*?\d.*%"): {},
                    ("全程接种率", r"覆盖人数和全程接种人数分别.*?\d.*%.*?(\d.*%)"): {},
                    ("加强免疫接种", r"加强免疫接种.*?(\d.*?万)"): {},
                },
            },
        }

    downloader.run(files, parallel_num=1)
