import subprocess
import threading
import multiprocessing
from multiprocessing.pool import ThreadPool
from pprint import pprint, pformat
import logging
import os
import json
import sys
from pathlib import Path
import re
import shutil
import textwrap
import urllib.parse
import urllib.request
import functools
import time
import traceback
from functools import reduce
import operator
import io
import csv
import random
import uuid
import atexit

import requests
import bs4
from lxml import html

# dev only
try:
    from icecream import ic
    import pysnooper
except:
    pass


class Downloader:
    """the download manager
    role to play:
    * match filename to user defined object
    * parallel download
    """

    execution_timeout = 5 * 60
    retry_count = 5
    retry_delay = 5

    def __init__(self):
        log_formatter = logging.Formatter(
            fmt="%(asctime)s [%(levelname)s:%(name)s:%(funcName)s] %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
        log_level = (
            logging.DEBUG
            if "--debug" in sys.argv or str(os.getenv("LOG_LEVEL")).upper() == "DEBUG"
            else logging.INFO
        )
        log_handler = logging.StreamHandler(sys.stdout)
        log_handler.setFormatter(log_formatter)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(log_level)
        self.logger.addHandler(log_handler)
        if log_level != logging.DEBUG:
            self.logger.info("to see full debug output: export LOG_LEVEL=DEBUG")

        self.download_mapping_file = "./options.json"
        self.regex_mapping = {}
        self.files = []
        self.download_dir = "./downloads"

    def match_filename(self):
        for filename_regex, file_class in self.regex_mapping.items():
            for filename in self.download_mapping_dict.keys():
                filename_match = re.search(filename_regex, Path(filename).stem)
                if filename_match:
                    self.logger.debug(
                        "matched file: %s for class %s", filename, file_class
                    )
                    file_metadata = {}
                    file_metadata.update(self.download_mapping_dict[filename])
                    file_metadata.update({"filename": filename})
                    file = file_class(file_metadata)
                    file.download_dir = self.download_dir
                    file.run = retry(self.retry_count, self.retry_delay)(file.run)
                    self.files.append(file)

    def source(self, filename_regex):
        def add_regex_mapping(file_class):
            self.regex_mapping[filename_regex] = file_class
            return file_class

        return add_regex_mapping

    def start_execution_time_watcher(self):
        def watcher_func(timeout):
            while timeout:
                time.sleep(1)
                timeout -= 1
            self.logger.error(
                f"downloader timeout after {self.execution_timeout}, exiting...\n"
            )
            os._exit(1)

        timeout = self.execution_timeout
        watcher_thread = threading.Thread(
            target=watcher_func, args=(timeout,), daemon=True
        )
        watcher_thread.start()

    def run(self, download_mapping_dict={}, parallel_num=1):
        self.start_execution_time_watcher()
        self.download_mapping_dict = download_mapping_dict
        if not self.download_mapping_dict:
            with open(self.download_mapping_file, "r") as fpr:
                self.download_mapping_dict = json.load(fpr).get("files")
        self.logger.debug("download_mapping: \n%s", pformat(self.download_mapping_dict))
        self.match_filename()

        shutil.rmtree(self.download_dir, ignore_errors=True)
        os.mkdir(self.download_dir)

        # default use cpu_count for parallel_num
        parallel_num = parallel_num or multiprocessing.cpu_count()
        pool = ThreadPool(parallel_num)  # init multiprocessing downloader pool
        jobs = [pool.apply_async(file.run, ()) for file in self.files]

        # make Exception thrown correctly in multiprocessing Pool
        # https://stackoverflow.com/questions/6728236/exception-thrown-in-multiprocessing-pool-not-detected
        for job in jobs:
            job.get()


class PreDownloadRunnerComponent:
    def run_predownload(self):
        if hasattr(self, "pre_download"):
            self.file_metadata = self.pre_download(self.file_metadata)


class PostDownloadRunnerComponent:
    def run_postdownload(self, metadata, filepath=None):
        filepath = filepath or os.path.join(self.download_dir, metadata["filename"])
        if hasattr(self, "post_download"):
            self.post_download(filepath)


def retry(retry_count, delay):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            for _ in range(retry_count):
                try:
                    return f(*args, **kwargs)
                except Exception as ex:
                    print(traceback.format_exc())
                    time.sleep(delay)

        return wrapper

    return decorator


class RunnerComponent(PreDownloadRunnerComponent, PostDownloadRunnerComponent):
    def run(self):
        """
        this method will try to run two user or module
        defined function:

        ```
        pre_download: optional function to call before download actually starts,
        (you can do like insert date string to url, get file download link
        from a webpage ..etc)

        Args:
            self: object
            session: object
                use like requests
            metadata: dict
                contains file mappings (filename, url ..etc)
        Returns:
            metadata: dict
                file mappings will be consumed
        ```

        ```
        post_download: optional function to call after download
        Args:
            downloaded_file_path: str
                file path of the downloaded file
        ```
        """
        if not isinstance(self.file_metadata, list):
            # self.file_metadata is not a list, means argen(if exists) is not done yet, then we do it (again) (if failed) in pre_download
            self.run_predownload()
        if isinstance(self.file_metadata, dict):
            self.file_metadata = [self.file_metadata]

        while True:
            if not self.file_metadata:
                return
            metadata = self.file_metadata[0]
            filename = metadata["filename"]
            url = metadata["url"]
            self.logger.info("downloading %s", filename)
            self.logger.info("url: %s", url)
            filepath = os.path.join(self.download_dir, filename)
            self.download(url, filepath)
            self.run_postdownload(metadata)
            self.file_metadata.pop(0)


class BaseComponent:
    retry_count = 5
    retry_delay = 5
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.82 Safari/537.36",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "Windows",
    }

    def __init__(self, file_metadata):
        log_formatter = logging.Formatter(
            fmt="%(asctime)s [%(levelname)s:%(name)s:%(funcName)s] %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
        log_level = (
            logging.DEBUG
            if "--debug" in sys.argv or str(os.getenv("LOG_LEVEL")).upper() == "DEBUG"
            else logging.INFO
        )
        log_handler = logging.StreamHandler(sys.stdout)
        log_handler.setFormatter(log_formatter)

        self.file_metadata = file_metadata
        self.logger = logging.getLogger(self.file_metadata["filename"])
        self.logger.setLevel(log_level)
        self.logger.addHandler(log_handler)

    def html_document_from_url(self, url):
        tmp_file_path = f"./tmp/tmp_{uuid.uuid4().hex}"
        if not Path(tmp_file_path).parent.exists():
            Path(tmp_file_path).parent.mkdir(parents=True)
        self.download(url, tmp_file_path)

        with open(tmp_file_path, "r") as fpr:
            html_str = fpr.read()
        html_doc = html.fromstring(html_str)
        return html_doc


class Requests(BaseComponent, RunnerComponent):
    """
    utilize python `requests` module
    """

    params = None
    auth = None
    cookies = None
    data = None
    proxies = None
    cert = None
    timeout = None
    verify = True
    # default value in `requests`: False
    stream = True
    files = None
    # always raise when error occurs
    hooks = {"response": lambda resp, *args, **kwargs: resp.raise_for_status()}
    json = None
    method = "GET"

    def __init__(self, file_metadata):
        super().__init__(file_metadata)

        # init session
        self.session = requests.Session()
        self.session.headers = self.headers
        self.session.params = self.params
        self.session.auth = self.auth
        self.session.cookies = requests.cookies.cookiejar_from_dict(self.cookies)
        self.session.proxies = self.proxies
        self.session.cert = self.cert
        self.session.verify = self.verify
        self.session.hooks = self.hooks

    def __format_response_detail(self):
        format_headers = lambda d: "\n".join(f"{k}: {v}" for k, v in d.items())
        return textwrap.dedent(
            """
        ---------------- response ---------------
        {resp.status_code} {resp.reason} {resp.url}
        {resp_headers}        
        """
        ).format(
            resp=self.response,
            resp_headers=format_headers(self.response.headers),
        )

    def __format_request_detail(self):
        format_headers = lambda d: "\n".join(f"{k}: {v}" for k, v in d.items())
        return textwrap.dedent(
            """
        ---------------- request ----------------
        {req.method} {req.url}
        {req_headers}
        
        {req.body}
        """
        ).format(
            req=self.prepared_request,
            req_headers=format_headers(self.prepared_request.headers),
        )

    def __send_request(self, url):
        self.request = requests.Request(
            method=self.method,
            url=url,
            files=self.files,
            data=self.data,
            json=self.json,
        )
        self.prepared_request = self.session.prepare_request(self.request)
        self.logger.debug("request detail: \n%s", self.__format_request_detail())
        self.response = self.session.send(
            request=self.prepared_request,
            stream=self.stream,
            timeout=self.timeout,
        )
        self.logger.info("status_code: %s", self.response.status_code)
        self.logger.debug("response detail: \n%s", self.__format_response_detail())

    def __save(self, filepath):
        with open(filepath, "wb") as fpw:
            self.response.raw.decode_content = True
            shutil.copyfileobj(self.response.raw, fpw)
        self.logger.info("downloaded to %s", filepath)

    def download(self, url, filepath):
        self.__send_request(url)
        self.__save(filepath)


class Urllib(BaseComponent, RunnerComponent):
    def download(self, url, filepath):
        request = urllib.request.Request(url=url, headers=self.headers)
        with urllib.request.urlopen(request) as response:
            self.logger.info("status_code: %s", response.status)
            with open(filepath, "wb") as fpw:
                shutil.copyfileobj(response, fpw)
            self.logger.info("downloaded to %s", filepath)


class LocalFile(BaseComponent, RunnerComponent):
    def download(self, src_file, filepath):
        shutil.copyfile(src_file, filepath)
        self.logger.info("downloaded to %s", filepath)


class Curl(BaseComponent, RunnerComponent):
    def download(self, url, filepath):
        format_headers = lambda d: " ".join(f"-H '{k}: {v}'" for k, v in d.items())
        curl_cmd = f"curl -L -s {url} {format_headers(self.headers)} -o {filepath}"
        self.logger.debug(f"curl cmd: {curl_cmd}")
        os.system(curl_cmd)
        self.logger.info("downloaded to %s", filepath)


# this downloader class currently only suitable for html page downloading
class CloudPlaywright(BaseComponent, RunnerComponent):
    # playwright api gateway
    cloud_playwright_api_url_list = [
        "https://normal-playwright-api-us.herokuapp.com/post",
        "https://normal-playwright-api-eloco.cloud.okteto.net/post",
    ]
    cloud_playwright_browser = "firefox"
    cloud_playwright_code = None

    def download(self, url, filepath):
        playwright_code = (
            self.cloud_playwright_code
            or f"page.goto('{url}',wait_until='commit'); time.sleep(10);result=page.content()"
        )
        self.logger.debug(f"playwright_code: {playwright_code}")

        form_data = {
            "run": playwright_code,
            "browser": self.cloud_playwright_browser,
            "stealth": "False",
        }

        playwright_api_url = random.choice(self.cloud_playwright_api_url_list)

        self.logger.info(
            f"requesting html with playwright api endpoint {playwright_api_url}"
        )
        resp = requests.post(playwright_api_url, data=form_data)
        resp.raise_for_status()
        self.logger.info("status_code: %s", resp.status_code)

        html_str = json.loads(resp.text)["result"]
        self.logger.debug(f"html string: \n{html_str}")

        with open(filepath, "w") as fpw:
            fpw.write(html_str)
        self.logger.info("downloaded to %s", filepath)


class Unzip:
    def post_download(self, downloaded_file_path):
        self.logger.info(f"unzipping {downloaded_file_path}..")
        download_dir = os.path.dirname(downloaded_file_path)
        unzip_cmd = f"7z x {downloaded_file_path} -y -o{download_dir}"
        self.logger.debug(f"unzip_cmd: {unzip_cmd}")
        os.system(unzip_cmd)


class NewsPaperRegexMatch:
    # desc starts with __ will be ignored in desc concating
    desc_regex_dict = {}
    csv_header = ["desc", "value"]
    regex_match_input_text = None
    regex_match_flags = None

    @staticmethod
    def dict_walk(remaining_dict, desc_list=[]):
        values = []
        for desc_regex_tuple in remaining_dict.keys():
            assert len(desc_regex_tuple) == 2
            next_level = remaining_dict[desc_regex_tuple]
            if not next_level:
                regex = desc_regex_tuple[1]
                if regex:
                    values.append([desc_list + [desc_regex_tuple]])
            elif isinstance(next_level, dict):
                values.append(
                    NewsPaperRegexMatch.dict_walk(
                        next_level, desc_list + [desc_regex_tuple]
                    )
                )
        return list(reduce(operator.iconcat, values, []))

    def __desc_concat(self, desc_regex_tuples_list):
        desc_regex_list = []
        for desc_regex_tuples in desc_regex_tuples_list:
            regex_list = list(map(lambda t: t[1], desc_regex_tuples))
            desc_list = list(
                map(lambda t: t[0], desc_regex_tuples),
            )
            desc_regex_list.append([desc_list, regex_list])
        return desc_regex_list

    @staticmethod
    def generate_csv_str_from_rows(rows, header=[]):
        with io.StringIO() as csv_str_fpw:
            csv_writer = csv.writer(
                csv_str_fpw, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
            )
            csv_writer.writerow(header)
            for row in rows:
                csv_writer.writerow(row)
            return csv_str_fpw.getvalue()

    def __regex_match(self, desc_regex_dict, input_str):
        desc_regex_tuples_list = NewsPaperRegexMatch.dict_walk(desc_regex_dict)
        desc_regex_list = self.__desc_concat(desc_regex_tuples_list)
        regex_match_flags = self.regex_match_flags or 0

        result_rows = []
        regex_match_cache = {}
        for desc_regex in desc_regex_list:
            desc_list = desc_regex[0]
            regex_list = desc_regex[1]
            result_str = input_str

            for regex_idx, regex in enumerate(regex_list):
                self.logger.debug(f"\nregex: {regex}\ntext: {result_str}\n")

                cached_result_str = regex_match_cache.get(
                    tuple(regex_list[: regex_idx + 1])
                )
                if cached_result_str:
                    if cached_result_str == "match_failed":
                        result_str = f"error: cannot match {regex}"
                        break
                    self.logger.debug(
                        f"use cached match result:\n{cached_result_str}\n"
                    )
                    result_str = cached_result_str
                    continue

                if isinstance(regex, tuple):
                    for single_regex in regex:
                        if not single_regex:
                            continue
                        match = re.search(
                            single_regex, result_str, flags=regex_match_flags
                        )
                        if match:
                            break
                else:
                    match = re.search(regex, result_str, flags=regex_match_flags)

                if not match:
                    context_str = (
                        result_str
                        if len(result_str) <= 100
                        else result_str[0:100] + ".. (full context in debug log)"
                    )
                    regex_str = regex if type(regex) == str else " | ".join(regex)
                    desc_str = ":".join(desc_list[: regex_idx + 1])
                    self.logger.warning(
                        f"cannot match regex: {regex_str}\ncontext: {context_str}\ndesc: {desc_str}\n"
                    )
                    result_str = f"error: cannot match {regex}"
                    regex_match_cache[
                        tuple(regex_list[: regex_idx + 1])
                    ] = "match_failed"
                    break

                result_str = match.group(1)
                regex_match_cache[tuple(regex_list[: regex_idx + 1])] = result_str

            desc_list = list(filter(lambda desc: not desc.startswith("__"), desc_list))
            desc = ":".join(desc_list[: regex_idx + 1])
            self.logger.debug(f"result:\ndesc: {desc}\ncaptured: {result_str}\n")

            row = [desc, result_str]
            result_rows.append(row)
        return result_rows

    def post_download(self, downloaded_file_path):
        with open(downloaded_file_path, "r") as fpr:
            html_str = fpr.read()
        regex_match_input_text = bs4.BeautifulSoup(html_str, features="lxml").get_text()
        regex_match_input_text = "\n".join(
            filter(lambda l: l, regex_match_input_text.splitlines())
        )
        regex_match_input_text = self.regex_match_input_text or regex_match_input_text

        self.logger.info("matching regexs in desc_regex_dict ...")
        csv_str = self.generate_csv_str_from_rows(
            self.__regex_match(self.desc_regex_dict, regex_match_input_text),
            header=self.csv_header,
        )
        self.logger.debug(f"regex match result: {csv_str}")
        with open(downloaded_file_path, "w") as fpw:
            fpw.write(csv_str)
        self.logger.info(f"result csv file written at {downloaded_file_path}")


class Pdf2Xlsx:
    pdf2xlsx_endpoint_list = [
        "http://dpa-cn-cdc19nhc.vpc.ceicdata.com:8081/pdf2excel",
    ]
    pdf2xlsx_draw_assist_line = True
    pdf2xlsx_min_number_count_of_a_data_row = 1

    # see https://camelot-py.readthedocs.io/en/master/user/advanced.html for some of below arguments meaning
    pdf2xlsx_flavor = "lattice"
    pdf2xlsx_lattice_line_scale = 15

    def post_download(self, downloaded_file_path):
        pdf2xlsx_endpoint = random.choice(self.pdf2xlsx_endpoint_list)
        self.logger.info(
            f"converting pdf to xlsx with `pdfhero` endpoint {pdf2xlsx_endpoint} ..."
        )

        data = {
            "token": "bpa",
            "draw_assist_line": self.pdf2xlsx_draw_assist_line,
            "flavor": self.pdf2xlsx_flavor,
        }
        if self.pdf2xlsx_flavor == "lattice":
            data["line_scale"] = self.pdf2xlsx_lattice_line_scale
        if self.pdf2xlsx_draw_assist_line:
            data[
                "min_numbers_of_a_data_row"
            ] = self.pdf2xlsx_min_number_count_of_a_data_row

        resp = requests.post(
            pdf2xlsx_endpoint,
            files={
                "pdf_file": (
                    Path(downloaded_file_path).name,
                    open(downloaded_file_path, "rb"),
                    "application/pdf",
                )
            },
            data=data,
        )
        resp.raise_for_status()
        self.logger.debug(f"pdf2xlsx endpoint resp:\n{pformat(resp.json())}")

        if resp.json().get("msg") != "success":
            self.logger.error("pdf2xlsx failed, see DEBUG log")
            return

        self.logger.info(
            "intermediate pdf: {}".format(resp.json()["result"]["pdf_file"])
        )

        xlsx_url = resp.json()["result"]["converted_file"]
        self.download(xlsx_url, downloaded_file_path)


class Cdp(BaseComponent, RunnerComponent):
    cdp_js_content = None
    cdp_show_log = False

    def download(self, url, filepath):
        tmp_filename = f"tmp_{uuid.uuid4().hex}"
        cdp_js_content = (
            self.cdp_js_content
            or """
    var genie = require('genie').create({{ xpathVersion: '2.0' }});
    genie.cdp.casper.options.timeout = 2 * 60 * 60 * 1000;
    var x = require('casper').selectXPath;
    var ex = genie.extendedXpath(x, {{ alphabet: 'en' }});
    var blacklist = ['.css', '.png', '.gif', '.jpeg', 'jpg'];
    
    
    var files1 = genie.files();
    files1.blacklist(blacklist)
        .retry(2)
        .add("{filename}")
        .steps(function (parameters) {{
            this.thenOpen("{url}")
            this.wait(1000)
        }});
    
    genie.run(files1);
    """.format(
                url=url, filename=tmp_filename
            )
        )

        os.makedirs("./tmp/downloads", exist_ok=True)
        with open("./tmp/cdp.js", "w") as f:
            f.write(cdp_js_content)

        cdp_stdout = None if self.cdp_show_log else subprocess.DEVNULL
        proc = subprocess.run(["sh", "-c", "cd ./tmp && cdp_run"], stdout=cdp_stdout)
        if proc.returncode != 0:
            self.logger.error(f"faild to download {tmp_filename} with cdp")
            return

        downloaded_file_path = os.path.join("./tmp/downloads", tmp_filename)
        if not os.path.exists(downloaded_file_path):
            self.logger.error(
                "cdp downloaded no file, please check it works separately"
            )
            raise Exception

        shutil.copyfile(downloaded_file_path, filepath)
        self.logger.info("downloaded to %s", filepath)
